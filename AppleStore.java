package �dev1;

public class AppleStore {                                                                         //our class
	String storeName;
	double exchangeRate;
	double macCost;
	double macPrice;
	int soldMacCount;

	 
	public AppleStore (double a, double b, double c, int d, String e) {                                     //constructor
		 exchangeRate = a;
		 macCost = b;
		 macPrice = c;
		 soldMacCount=d;
		 storeName=e;
	}
	
	
	
	
	
	
	public static void main(String[] args) {
    AppleStore MemleketStore = new AppleStore(7.50, 1200, 1500, 0, "Memleket Store");
    System.out.println(MemleketStore);
    MemleketStore.sellMacs(10);
    MemleketStore.sellMacs(5);
    MemleketStore.printStoreFinancials();
	}
	
	
	
	
	
	
	
	
	
	
	
	public  void sellMacs (int NumberOfMacsSold) {   // to change soldMacCount variable
		soldMacCount+=NumberOfMacsSold;
		System.out.println(NumberOfMacsSold + " macs sold");
		
	}		
	
	
	
	
	public  double setExchangeRate(double newrate) {                                       // to set new exchange rate
		System.out.println("The exchange rate has been changed!");
		System.out.println("The exchange rate: 1 USD = " + newrate + " TL");
		return newrate;
	}
	
	
	
	public  double getRevenue() {                 // to get revenue
		double revenue;
		revenue=macPrice*soldMacCount;
		return revenue;
	}
	
	
	
	public  double getProfit() {     // to get profit
		double profit;
		profit=(macPrice-macCost)*soldMacCount;
		return profit;
	}

	
	public  String toString() {        // to print the results...
	    String info =  "\nStore name: " + storeName +
	    		"\nThe cost of the Macbook: " + macCost  + " USD"
	    		+ "\nThe price of the Macbook: " + macPrice + " USD" +
	    		"\nThe exchange rate: 1 USD = " + exchangeRate  + " TL";
	    
		return 	info;
	}
	
	public  void printStoreFinancials() {
		System.out.println("Total number of macs sold: " + soldMacCount);
		System.out.println("Total revenue: " + getRevenue()*exchangeRate + " TL");
		System.out.println("Total profit:" + getProfit()*exchangeRate + " TL");
	}
}








